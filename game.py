from random import randint

#number_between_1_and_100 = randint(1,100)
#number_between_8_and_16 = randint(8,16)

name = input("What is your name? ")
month = randint(1,12)
year = randint(1924,2004)

for guess_number in range(1,6):
    print("Guess " + str(guess_number) + ": " + str(name) + ", were you born on " + str(month) + "/" + str(year) + "?")
    answer = input("yes or no ")

    if answer == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
